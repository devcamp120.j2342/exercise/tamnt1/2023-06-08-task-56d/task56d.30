package com.example.artistalbumapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.artistalbumapi.models.Album;
import com.example.artistalbumapi.models.Artist;
import com.example.artistalbumapi.services.AlbumService;
import com.example.artistalbumapi.services.ArtistService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ArtistController {
    @Autowired
    private ArtistService artistService;

    @GetMapping("/artist")
    public List<Artist> getArtistList() {

        return artistService.createArtistList();
    }

    @GetMapping("/artist-info")
    public Artist getArtistInfo(@RequestParam int artistId) {
        List<Artist> artistList = artistService.createArtistList();
        return artistService.getArtistInfo(artistId, (ArrayList<Artist>) artistList);
    }

    @GetMapping(value = "/artist/{index}")
    public Artist getMethodName(@PathVariable("index") int index) {
        return artistService.getArtistByIndex(index);

    }
}
