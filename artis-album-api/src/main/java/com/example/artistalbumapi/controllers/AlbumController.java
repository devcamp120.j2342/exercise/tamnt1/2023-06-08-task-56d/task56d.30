package com.example.artistalbumapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.artistalbumapi.models.Album;
import com.example.artistalbumapi.services.AlbumService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @GetMapping("/album-info")
    public Album getAlbumInfo(@RequestParam int albumId) {
        List<Album> albumList = albumService.createAlbumList();
        System.out.println(albumList);
        return albumService.getAlbumInfomation(albumId, (ArrayList<Album>) albumList);
    }
}
