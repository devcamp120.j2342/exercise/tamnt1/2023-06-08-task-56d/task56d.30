package com.example.artistalbumapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArtisAlbumApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArtisAlbumApiApplication.class, args);
	}

}
