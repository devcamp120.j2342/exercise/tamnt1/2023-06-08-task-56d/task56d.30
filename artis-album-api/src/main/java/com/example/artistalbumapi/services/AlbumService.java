package com.example.artistalbumapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.artistalbumapi.models.Album;

@Service
public class AlbumService {
    public ArrayList<Album> createAlbumList() {
        ArrayList<Album> albumList = new ArrayList<>();
        ArrayList<String> songsList1 = new ArrayList<>();
        songsList1.addAll(Arrays.asList("better", "minty", "aloha"));
        ArrayList<String> songsList2 = new ArrayList<>();
        songsList2.addAll(Arrays.asList("the bad", "gonny", "lost"));

        ArrayList<String> songsList3 = new ArrayList<>();
        songsList3.addAll(Arrays.asList("herry", "aleni", "binzi"));

        Album album1 = new Album(1, "The Rock", songsList1);
        Album album2 = new Album(2, "The Main", songsList2);
        Album album3 = new Album(3, "The Codiack", songsList3);
        albumList.addAll(Arrays.asList(album1, album2, album3));

        for (Album album : albumList) {
            System.out.println(album);
        }
        return albumList;

    }

    public Album getAlbumInfomation(int id, ArrayList<Album> albumList) {
        for (Album album : albumList) {
            if (album.getId() == id) {
                return album;
            }
        }
        return null;
    }
}
