package com.example.artistalbumapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.artistalbumapi.models.Album;
import com.example.artistalbumapi.models.Artist;

@Service
public class ArtistService {
    @Autowired
    private AlbumService albumService;

    public List<Artist> createArtistList() {

        ArrayList<Album> albumList = albumService.createAlbumList();
        ArrayList<Artist> artistList = new ArrayList<>();
        Artist artist1 = new Artist(1, "Taylor", albumList);
        Artist artist2 = new Artist(2, "Mint", albumList);
        Artist artist3 = new Artist(3, "Adam", albumList);
        Artist artist4 = new Artist(4, "Sin", albumList);
        Artist artist5 = new Artist(5, "Mike", albumList);
        Artist artist6 = new Artist(5, "Peter", albumList);
        artistList.addAll(Arrays.asList(artist1, artist2, artist3, artist4, artist5, artist6));

        for (Artist artist : artistList) {
            System.out.println(artist);
        }
        return artistList;
    }

    public Artist getArtistInfo(int id, ArrayList<Artist> artistList) {
        for (Artist artist : artistList) {
            if (artist.getId() == id) {
                return artist;
            }
        }
        return null;
    }

    public Artist getArtistByIndex(int idx) {
        ArrayList<Artist> artistList = (ArrayList<Artist>) createArtistList();

        for (int index = 0; index < artistList.size(); index++) {
            if (index >= 0 && index < artistList.size()) {
                return artistList.get(index);
            }
        }
        return null;
    }
}
